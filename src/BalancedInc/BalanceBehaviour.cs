﻿using BalancedInc.Balancers;
using System.Collections.Generic;
using System.IO;

namespace BalancedInc
{
    public class BalanceBehaviour : ModBehaviour, IConfiguration, IStatusProvider
    {
        protected ILogger Logger { get; set; }

        protected IEnumerable<ICostBalancer> CostBalancers { get; set; }

        public string Status { get; set; } = "Activate me!";

        public BalanceBehaviour()
        {
            Logger = new StatusLogger(this);
            CostBalancers = new List<ICostBalancer>()
            {
                new FurnitureBalancer(Logger, this),
                new SegmentBalancer(Logger, this),
                new MarketingBalancer(Logger, this),
                new RoadBalancer(Logger, this),
                new WallBalancer(Logger, this)
            };

            // hack to prevent mod from being loaded when game starts since some of the 
            // objects that get balanced don't exist at the time OnActivate is called
            if (File.Exists("DLLMods/BalanceSetting.txt"))
                File.Delete("DLLMods/BalanceSetting.txt");
        }

        public override void OnActivate()
        {            
            Logger.Log("Activating mod...");
            Logger.Log("");
            
            foreach (ICostBalancer balancer in CostBalancers)
            {
                balancer.Balance();
            }
            
            Logger.Log("Activation complete.");
        }

        public override void OnDeactivate()
        {
            Logger.ClearHistory();

            Logger.Log("Deactivating mod...");
            Logger.Log("");

            foreach (ICostBalancer balancer in CostBalancers)
            {
                balancer.Reset();
            }

            Logger.Log("Deactivation complete.");
        }
    }
}
