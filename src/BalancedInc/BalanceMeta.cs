﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BalancedInc
{
    public class BalanceMeta : ModMeta
    {
        public Text InfoText { get; protected set; }

        public string Name
        {
            get
            {
                return "Balance Mod";
            }
        }

        
        public void ConstructOptionsScreen(RectTransform parent, ModBehaviour[] behaviours)
        {
            var versionLabel = WindowManager.SpawnLabel();
            versionLabel.text = "Version 0.9";

            var lastTestedGameVersionLabel = WindowManager.SpawnLabel();
            lastTestedGameVersionLabel.text = "For Alpha 8.10.13";

            InfoText = WindowManager.SpawnLabel();
            InfoText.text = "Not Loaded";

            var status = InfoText.gameObject.AddComponent<ModStatusText>();
            status.Provider = behaviours.OfType<BalanceBehaviour>().First();

            WindowManager.AddElementToElement(versionLabel.gameObject, parent.gameObject, new Rect(10, 10, 256, 20), new Rect(0, 0, 0, 0));
            WindowManager.AddElementToElement(lastTestedGameVersionLabel.gameObject, parent.gameObject, new Rect(10, 30, 256, 20), new Rect(0, 0, 0, 0));
            WindowManager.AddElementToElement(InfoText.gameObject, parent.gameObject, new Rect(10, 70, 256, 2000), new Rect(0, 0, 0, 0));
        }
    }
}
