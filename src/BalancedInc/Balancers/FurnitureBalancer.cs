﻿using System;

namespace BalancedInc.Balancers
{
    public class FurnitureBalancer : ICostBalancer
    {        
        protected ILogger Logger { get; set; }

        protected IConfiguration Settings { get; set; }

        public FurnitureBalancer(ILogger logger, IConfiguration settings)
        {
            Logger = logger;
            Settings = settings;
        }

        public virtual void Balance()
        {
            Logger.Log("Balancing furniture...");

            foreach (var go in ObjectDatabase.Instance.GetAllFurniture())
            {
                try
                {
                    Furniture furniture = go.GetComponent<Furniture>();
                    if (furniture != null)
                    {
                        Logger.Log(" " + furniture.name);
                        
                        // cache stock cost so it can be reset if user deactivates
                        Settings.SaveSetting("StockCostOf" + furniture.name.Replace(" ", ""), furniture.Cost.ToString());

                        // calculate balanced cost for furniture item
                        furniture.Cost = CalcCost(furniture);
                    }
                    else
                        Logger.Log(" Error: No Furniture component for game object '" + go.name + "'.");

                }
                catch (Exception ex)
                {
                    Logger.Log(" Error: Unexpected Exception occurred while balancing furniture.");                    
                }
            }

            Logger.Log("Done.");
            Logger.Log("");
        }

        public virtual void Reset()
        {
            Logger.Log("Restoring furniture to stock cost...");

            foreach (var go in ObjectDatabase.Instance.GetAllFurniture())
            {
                try
                {
                    Furniture furniture = go.GetComponent<Furniture>();
                    if (furniture != null)
                    {
                        Logger.Log(" " + furniture.name);

                        // load cached stock cost
                        furniture.Cost = Settings.LoadSetting("StockCostOf" + furniture.name.Replace(" ", ""), furniture.Cost);                        
                    }
                    else
                        Logger.Log(" Error: No Furniture component for game object '" + go.name + "'.");

                }
                catch (Exception ex)
                {
                    Logger.Log(" Error: Unexpected Exception occurred while restoring furniture cost.");
                }
            }

            Logger.Log("Done.");
            Logger.Log("");
        }

        protected virtual float CalcCost(Furniture furniture)
        {
            float cost = furniture.Cost; // default to stock cost

            switch (furniture.name)
            {
                case "Elevator":
                    cost = 10000;
                    break;
                case "Stairs":
                    cost = 2500;
                    break;
                case "Cheap Chair":
                    cost = 50;
                    break;
                case "Office Chair":
                    cost = 200;
                    break;
                case "Waiting Chairs":
                    cost = 250;
                    break;
                case "Couch":
                    cost = 500;
                    break;
                case "Bench":
                    cost = 350;
                    break;
                case "Small Heater":
                    cost = 75;
                    break;
                case "Ceiling Fan":
                    cost = 200;
                    break;
                case "Radiator":
                    cost = 200;
                    break;
                case "Central Heating":
                    cost = 5000;
                    break;
                case "AC Unit":
                    cost = 5000;
                    break;
                case "Industrial ventilation":
                    cost = 500;
                    break;
                case "Old Computer":
                    // TODO: make dynamic by year
                    cost = 2000;
                    break;
                case "90s Computer":
                    // TODO: make dynamic by year
                    cost = 2000;
                    break;
                case "Laptop":
                    // TODO: make dynamic by year
                    cost = 2000;
                    break;
                case "Modern Computer":
                    // TODO: make dynamic by year
                    cost = 2000;
                    break;
                case "HoloComputer":
                    // TODO: make dynamic by year
                    cost = 2000;
                    break;
                case "Cubicle wall":
                    cost = 150;
                    break;
                case "Lamp":
                    cost = 100;
                    break;
                case "Wall Lamp":
                    cost = 100;
                    break;
                case "Desk Lamp":
                    cost = 50;
                    break;
                case "Floor Lamp":
                    cost = 65;
                    break;
                case "Vending Machine":
                    cost = 3000;
                    break;
                case "Fridge":
                    cost = 1300;
                    break;
                case "Stove":
                    cost = 700;
                    break;
                case "Serving Tray":
                    cost = 50;
                    break;
                case "Espresso Machine":
                    cost = 250;
                    break;
                case "Toilet":
                    cost = 200;
                    break;
                case "Watercooler":
                    cost = 140;
                    break;
                case "TV":
                    cost = 2000;
                    break;
                case "Reception desk":
                    cost = 1000;
                    break;
                case "Floor Plant":
                    cost = 60;
                    break;
                case "Table Plant":
                    cost = 25;
                    break;
                case "Table Cactus":
                    cost = 25;
                    break;
                case "Big Plant":
                    cost = 75;
                    break;
                case "Clock":
                    cost = 20;
                    break;
                case "Small Server":
                    // TODO: make dynamic by year
                    cost = 3000;
                    break;
                case "Medium Server":
                    // TODO: make dynamic by year
                    cost = 4000;
                    break;
                case "Tower Server":
                    // TODO: make dynamic by year
                    cost = 5000;
                    break;
                case "Server Rack":
                    // TODO: make dynamic by year
                    cost = 10000;
                    break;
            }

            return cost;
        }        
    }
}
