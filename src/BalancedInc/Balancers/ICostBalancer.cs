﻿namespace BalancedInc.Balancers
{
    public interface ICostBalancer
    {
        void Balance();

        void Reset();
    }
}
