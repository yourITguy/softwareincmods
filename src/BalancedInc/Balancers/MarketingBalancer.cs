﻿namespace BalancedInc.Balancers
{
    public class MarketingBalancer : ICostBalancer
    {
        protected ILogger Logger { get; set; }

        protected IConfiguration Settings { get; set; }

        public MarketingBalancer(ILogger logger, IConfiguration settings)
        {
            Logger = logger;
            Settings = settings;
        }

        public void Balance()
        {
            Logger.Log("Balancing marketing...");

            Settings.SaveSetting("StockCostOfMarketing", MarketingWindow.CostPerUnit.ToString());

            MarketingWindow.CostPerUnit *= 100;

            Logger.Log("Done.");
            Logger.Log("");
        }

        public void Reset()
        {
            Logger.Log("Restoring marketing to stock cost...");

            MarketingWindow.CostPerUnit = Settings.LoadSetting("StockCostOfMarketing", MarketingWindow.CostPerUnit);

            Logger.Log("Done.");
            Logger.Log("");
        }
    }
}
