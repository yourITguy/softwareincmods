﻿namespace BalancedInc.Balancers
{
    public class RoadBalancer : ICostBalancer
    {
        protected ILogger Logger { get; set; }

        protected IConfiguration Settings { get; set; }

        public RoadBalancer(ILogger logger, IConfiguration settings)
        {
            Logger = logger;
            Settings = settings;
        }

        public virtual void Balance()
        {
            Logger.Log("Balancing roads...");

            Settings.SaveSetting("StockCostOfRoads", RoadBuildCube.roadCost.ToString());

            RoadBuildCube.roadCost = 1500;

            Logger.Log("Done.");
            Logger.Log("");
        }

        public virtual void Reset()
        {
            Logger.Log("Restoring marketing to stock cost...");

            RoadBuildCube.roadCost = Settings.LoadSetting("StockCostOfRoads", RoadBuildCube.roadCost);

            Logger.Log("Done.");
            Logger.Log("");
        }
    }
}
