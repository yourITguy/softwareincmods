﻿using System;

namespace BalancedInc.Balancers
{
    public class SegmentBalancer : ICostBalancer
    {
        protected RoomSegment Segment { get; set; }

        protected ILogger Logger { get; set; }

        protected IConfiguration Settings { get; set; }

        public SegmentBalancer(ILogger logger, IConfiguration settings)
        {            
            Logger = logger;
            Settings = settings;
        }

        public virtual void Balance()
        {
            Logger.Log("Balancing segments...");
            foreach (var go in ObjectDatabase.Instance.RoomSegments)
            {
                try
                {                    
                    var segment = go.GetComponent<RoomSegment>();
                    if (segment != null)
                    {
                        Logger.Log(" " + segment.name);

                        // cache stock cost so it can be reset if user deactivates
                        Settings.SaveSetting("StockCostOf" + segment.name.Replace(" ", ""), segment.Cost.ToString());

                        // calculate balanced cost for furniture item
                        segment.Cost = CalcCost(segment);
                    }
                    else
                        Logger.Log(" Error: No RoomSegment component for game object '" + go.name + "'.");
                }
                catch (Exception ex)
                {
                    Logger.Log(" Error: Unexpected Exception occurred while balancing segments.");
                }
            }

            Logger.Log("Done.");
            Logger.Log("");
        }

        public virtual void Reset()
        {
            Logger.Log("Restoring segments to stock cost...");

            foreach (var go in ObjectDatabase.Instance.RoomSegments)
            {
                try
                {
                    RoomSegment segment = go.GetComponent<RoomSegment>();
                    if (segment != null)
                    {
                        Logger.Log(" " + segment.name);

                        // load cached stock cost
                        segment.Cost = Settings.LoadSetting("StockCostOf" + segment.name.Replace(" ", ""), segment.Cost);
                    }
                    else
                        Logger.Log(" Error: No RoomSegment component for game object '" + go.name + "'.");

                }
                catch (Exception ex)
                {
                    Logger.Log(" Error: Unexpected Exception occurred while restoring segments cost.");
                }
            }

            Logger.Log(" Done.");
            Logger.Log("");
        }

        protected virtual float CalcCost(RoomSegment segment)
        {
            float cost = segment.Cost;

            switch (segment.name)
            {                
                case "Standard door":
                    cost = 100;
                    break;
                case "Sliding glass door":
                    cost = 300;
                    break;
                case "Double doors":
                    cost = 300;
                    break;
                case "Double glass door":
                    cost = 900;
                    break;
                case "Small window":
                    cost = 90;
                    break;
                case "Large window":
                    cost = 180;
                    break;
                case "Wide window":
                    cost = 270;
                    break;
                case "Arched gate":
                    cost = 200;
                    break;
                case "Open gate":
                    cost = 50;
                    break;
            }

            return cost;
        }        
    }
}
