﻿namespace BalancedInc.Balancers
{
    public class WallBalancer : ICostBalancer
    {
        protected ILogger Logger { get; set; }

        protected IConfiguration Settings { get; set; }

        public WallBalancer(ILogger logger, IConfiguration settings)
        {
            Logger = logger;
            Settings = settings;
        }

        public void Balance()
        {
            Logger.Log("Balancing walls...");

            Settings.SaveSetting("StockCostOfWalls", BuildController.RoomPrice.ToString());
            Settings.SaveSetting("StockCostOfFences", BuildController.FencePrice.ToString());

            BuildController.RoomPrice = 1500;
            BuildController.FencePrice = 100;

            Logger.Log("Done.");
            Logger.Log("");
        }

        public void Reset()
        {
            Logger.Log("Restoring walls to stock cost...");

            BuildController.RoomPrice = Settings.LoadSetting("StockCostOfWalls", BuildController.RoomPrice);
            BuildController.FencePrice = Settings.LoadSetting("StockCostOfFences", BuildController.FencePrice);

            Logger.Log("Done.");
            Logger.Log("");
        }
    }
}
