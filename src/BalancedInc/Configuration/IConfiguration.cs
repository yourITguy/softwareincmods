﻿namespace BalancedInc
{
    public interface IConfiguration
    {
        void SaveSetting(string name, string value);

        T LoadSetting<T>(string name, T defaultValue);
    }
}
