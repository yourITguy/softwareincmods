﻿namespace BalancedInc
{
    public interface IStatusProvider
    {
        string Status { get; set; }
    }
}
