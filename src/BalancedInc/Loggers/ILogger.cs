﻿namespace BalancedInc
{
    public interface ILogger
    {
        void Log(string message);

        void ClearHistory();
    }
}
