﻿using System;
using System.Text;

namespace BalancedInc
{
    public class StatusLogger : ILogger
    {
        protected IStatusProvider StatusProvider { get; set; }

        protected StringBuilder Logs { get; set; }

        public StatusLogger(IStatusProvider statusProvider)
        {
            StatusProvider = statusProvider;
            Logs = new StringBuilder();
        }

        public void Log(string message)
        {
            Logs.AppendLine(message);
            StatusProvider.Status = Logs.ToString();
        }

        public void ClearHistory()
        {
            Logs.Clear();
        }
    }
}
