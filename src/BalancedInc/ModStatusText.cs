﻿using UnityEngine;
using UnityEngine.UI;

namespace BalancedInc
{
    public class ModStatusText : MonoBehaviour
    {
        public IStatusProvider Provider;

        private Text Label;

        void Start()
        {
            Label = GetComponent<Text>();
        }

        void Update()
        {
            if (Provider != null)
            {
                Label.text = Provider.Status;
            }
        }
    }
}
